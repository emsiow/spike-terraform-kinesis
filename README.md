# spike-terraform-kinesis

### Providing AWS credentials

Create a file with the extension `.auto.tfvars`, eg. `secrets.auto.tfvars`. Files with this extension have been configured to be ignored by Git in order to prevent exposing secrets through your version control (refer to the `.gitignore`)

Include the following contents in your file:

```
access_key = "ACCESS_KEY_HERE"
secret_key = "SECRET_KEY_HERE"
```

Replace the `ACCESS_KEY_HERE` and `SECRET_KEY_HERE` with an AWS access key and secret key.

### Building package for AWS lambda

After making changes to `lambda.js`, run `npm run build` to build the `.zip` package

Run `terraform apply` to have the package uploaded to S3
