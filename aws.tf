provider "aws" {
  ## if credentials are not included in provider block
  # terraform will default to searching for locally stored aws credentials
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "spike-terraform-kinesis-backend"
    key    = "terraform/state"
    region = "us-east-1" # cannot use string interpolation for backend config
  }
}

### resources to setup state backend to write to s3

resource "aws_s3_bucket" "state_backend_bucket" {
  bucket        = "spike-terraform-kinesis-backend"
  acl           = "private"

  versioning {
    enabled     = true
  }

  tags = {
    Name = "tf-state-backend"
    Environment = "Dev"
  }
}

resource "aws_iam_role" "state_backend_role" {
  name = "tf_state_backend_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name        = "tf-state-backend"
    Environment = "Dev"
  }
}

resource "aws_iam_role_policy" "state_backend_policy" {
  name = "tf_state_backend_policy"
  role = "${aws_iam_role.state_backend_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.state_backend_bucket.bucket}"
    },
    {
      "Effect": "Allow",
      "Action": ["s3:GetObject", "s3:PutObject"],
      "Resource": "arn:aws:s3:::${aws_s3_bucket.state_backend_bucket.bucket}"
    }
  ]
}
EOF
}

### bucket resource to store analytics

resource "aws_s3_bucket" "analytics_bucket" {
  bucket = "tf-analytics-bucket"
  acl    = "private"

  tags = {
    Name        = "tf-s3-bucket"
    Environment = "Dev"
    "custom"    = "custom tag example"
  }
  ## for test purposes
  force_destroy = true
}

resource "aws_s3_bucket_metric" "analytics_bucket_metric" {
  bucket = "${aws_s3_bucket.analytics_bucket.bucket}"
  name   = "S3BucketMetrics"
}

resource "aws_sns_topic" "analytics_bucket_low_activity_topic" {
  name = "s3_low_activity_topic"
}

resource "aws_cloudwatch_metric_alarm" "low_s3_activity" {
  alarm_name          = "low-activity-alert"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "2"
  metric_name         = "AllRequests"  # refer to AWS docs for available S3 metrics
  namespace           = "AWS/S3"
  period              = "60"
  statistic           = "Minimum"
  threshold           = "1"
  alarm_description   = "This metric alerts you when requests to S3 drop below 1 per min"
  alarm_actions       = ["${aws_sns_topic.analytics_bucket_low_activity_topic.arn}"]
  treat_missing_data  = "breaching"  # will interpret a lack of data as a logical lack of requests

  dimensions {
    BucketName        = "${aws_s3_bucket_metric.analytics_bucket_metric.bucket}"
    FilterId          = "${aws_s3_bucket_metric.analytics_bucket_metric.name}"
  }
}

### firehose delivery stream resources to deliver into s3 bucket

resource "aws_iam_role" "firehose_role" {
  name = "tf_firehose_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name        = "tf-firehose-stream"
    Environment = "Dev"
  }
}

resource "aws_iam_role_policy" "s3_policy" {
  name = "tf_s3_policy"
  role = "${aws_iam_role.firehose_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.analytics_bucket.bucket}/*"
        }
    ]
}
EOF
}

resource "aws_kinesis_firehose_delivery_stream" "firehose_stream" {
  name = "tf-kinesis-stream"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = "${aws_iam_role.firehose_role.arn}"
    bucket_arn = "${aws_s3_bucket.analytics_bucket.arn}"
    buffer_size = 1
    buffer_interval = 60

    cloudwatch_logging_options {
      enabled = true
      log_group_name = "/aws/kinesisfirehose/tf-kinesis-stream"
      log_stream_name = "S3Delivery"
    }
  }

  tags = {
    Name        = "tf-firehose-stream"
    Environment = "Dev"
  }
}

resource "aws_sns_topic" "firehose_high_activity_topic" {
  name = "firehose_high_activity_topic"
}

resource "aws_cloudwatch_metric_alarm" "high_firehose_activity" {
  alarm_name           = "high-firehose-activity-alert"
  comparison_operator  = "GreaterThanOrEqualToThreshold"
  evaluation_periods   = "2"
  metric_name          = "PutRecordBatch.Requests"  # refer to AWS docs for available Firehose metrics
  namespace            = "AWS/Firehose"
  period               = "60"
  statistic            = "Maximum"
  threshold            = "3"
  alarm_description    = "This metric alerts you when requests to put data into delivery stream exceeds 3 per min"
  alarm_actions        = ["${aws_sns_topic.firehose_high_activity_topic.arn}"]
  treat_missing_data   = "notBreaching"  # will interpret a lack of data as a logical lack of requests

  dimensions {
    DeliveryStreamName = "${aws_kinesis_firehose_delivery_stream.firehose_stream.name}"
  }
}

### lambda resource to ferry requests to firehose delivery stream

resource "aws_s3_bucket" "lambda_code_bucket" {
  bucket = "tf-lambda-code-bucket"
  acl    = "private"

  versioning {
    enabled = true
  }
  ## for test purposes
  force_destroy = true
}

resource "aws_s3_bucket_object" "lambda_code_source" {
  bucket = "${aws_s3_bucket.lambda_code_bucket.id}"
  key    = "lambda_code"
  source = "lambda.zip"
  etag   = "${md5(file("lambda.zip"))}"
}

resource "aws_iam_role" "lambda_role" {
  name = "tf_lambda_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "tf_lambda_policy"
  role = "${aws_iam_role.lambda_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "firehose:PutRecord",
                "firehose:PutRecordBatch"
            ],
            "Resource": "${aws_kinesis_firehose_delivery_stream.firehose_stream.arn}"
        }
    ]
}
EOF
}

resource "aws_lambda_function" "lambda_function" {
  function_name    = "tf-lambda-firehose"
  role             = "${aws_iam_role.lambda_role.arn}"
  handler          = "lambda.handler"
  runtime          = "nodejs8.10"
  s3_bucket        = "${aws_s3_bucket.lambda_code_bucket.id}"
  s3_key           = "${aws_s3_bucket_object.lambda_code_source.key}"
  s3_object_version = "${aws_s3_bucket_object.lambda_code_source.version_id}"

  environment {
    variables = {
      DELIVERY_STREAM_NAME = "${aws_kinesis_firehose_delivery_stream.firehose_stream.name}"
      IPDATA_API_KEY = "${var.ipdata_api_key}"
      USERSTACK_API_KEY = "${var.userstack_api_key}"
    }
  }
}

## api gateway resource to provide rest endpoint to track metrics

resource "aws_api_gateway_rest_api" "rest_api_gateway" {
  name        = "tf-rest-api-gateway"
  description = "REST API for analytics spike project"
}

resource "aws_api_gateway_resource" "analytics_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
  parent_id   = "${aws_api_gateway_rest_api.rest_api_gateway.root_resource_id}"
  path_part   = "analytics"
}

resource "aws_api_gateway_method" "analytics_options_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.analytics_resource.id}"
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "options_200" {
    rest_api_id   = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
    resource_id   = "${aws_api_gateway_resource.analytics_resource.id}"
    http_method   = "${aws_api_gateway_method.analytics_options_method.http_method}"
    status_code   = "200"
    response_models {
        "application/json" = "Empty"
    }
    response_parameters {
        "method.response.header.Access-Control-Allow-Headers" = true,
        "method.response.header.Access-Control-Allow-Methods" = true,
        "method.response.header.Access-Control-Allow-Origin" = true
    }
    depends_on = ["aws_api_gateway_method.analytics_options_method"]
}

resource "aws_api_gateway_integration" "options_integration" {
    rest_api_id             = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
    resource_id             = "${aws_api_gateway_resource.analytics_resource.id}"
    http_method             = "${aws_api_gateway_method.analytics_options_method.http_method}"
    type                    = "HTTP_PROXY"
    uri                     = "${aws_api_gateway_deployment.deployment.invoke_url}${aws_api_gateway_resource.analytics_resource.path}"
    integration_http_method = "POST"
    depends_on              = ["aws_api_gateway_method.analytics_options_method", "aws_api_gateway_deployment.deployment"]
}

resource "aws_api_gateway_integration_response" "options_integration_response" {
    rest_api_id   = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
    resource_id   = "${aws_api_gateway_resource.analytics_resource.id}"
    http_method   = "${aws_api_gateway_method.analytics_options_method.http_method}"
    status_code   = "${aws_api_gateway_method_response.options_200.status_code}"
    response_parameters = {
        "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
        "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS,POST,PUT'",
        "method.response.header.Access-Control-Allow-Origin" = "'*'"
    }
    depends_on = ["aws_api_gateway_method_response.options_200"]
}

resource "aws_api_gateway_method" "analytics_post_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.analytics_resource.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "cors_method_response_200" {
    rest_api_id   = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
    resource_id   = "${aws_api_gateway_resource.analytics_resource.id}"
    http_method   = "${aws_api_gateway_method.analytics_post_method.http_method}"
    status_code   = "200"
    response_models {
        "application/json" = "Empty"
    }
    response_parameters = {
        "method.response.header.Access-Control-Allow-Headers" = true,
        "method.response.header.Access-Control-Allow-Origin" = true
    }
    depends_on = ["aws_api_gateway_method.analytics_post_method"]
}

resource "aws_api_gateway_integration" "lambda_api_integration" {
  rest_api_id             = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.analytics_resource.id}"
  http_method             = "${aws_api_gateway_method.analytics_post_method.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/${aws_lambda_function.lambda_function.arn}/invocations"
}

resource "aws_api_gateway_deployment" "deployment" {
    rest_api_id   = "${aws_api_gateway_rest_api.rest_api_gateway.id}"
    stage_name    = "dev"
    depends_on    = ["aws_api_gateway_integration.lambda_api_integration"]
}

# api gateway integration with lambda function
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_function.arn}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.aws_account_id}:${aws_api_gateway_rest_api.rest_api_gateway.id}/*/${aws_api_gateway_method.analytics_post_method.http_method}${aws_api_gateway_resource.analytics_resource.path}"
}
