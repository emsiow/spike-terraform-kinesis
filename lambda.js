const axios = require('axios');
const AWS = require('aws-sdk');
const firehose = new AWS.Firehose();

function getLocation(ip) {
  return axios
    .get(`https://api.ipdata.co/${ip}/en?api-key=${process.env.IPDATA_API_KEY}`);
}

function getUserAgent(uaStr) {
  return axios
    .get(`http://api.userstack.com/detect?access_key=${process.env.USERSTACK_API_KEY}&ua=${uaStr}`);
}

exports.handler = (event, context, callback) => {
  var data = {...JSON.parse(event.body)};
  if (data.client) {
    data.client.ip = event.requestContext.identity.sourceIp;
    data.client.user_agent_string = event.requestContext.identity.userAgent;
    if (data.name === "project_open") {
      axios
        .all([getLocation(data.client.ip), getUserAgent(data.client.user_agent_string)])
        .then(axios.spread(function (loc, uaRes) {
          let ua = uaRes.data;
          data.client.location = {
            city: loc.data.city,
            continent: loc.data.continent_name,
            coordinates: [loc.data.longitude, loc.data.latitude],
            country: loc.data.country_name,
            country_code: loc.data.country_code,
            postal_code: loc.data.postal,
            province: loc.data.region
          };
          data.client.user_agent = {
            browser: {
              family: ua.browser.name,
              major: ua.browser.version_major
            },
            device: {
              family: `${ua.device.brand} ${ua.device.name}`
            },
            os: {
              family: ua.os.name
            }
          };
          let params = {
            DeliveryStreamName: process.env.DELIVERY_STREAM_NAME,
            Record: {
              Data: `${JSON.stringify(data)}\n`
            }
          };
          firehose.putRecord(params, (err, res) => {
            if (err) {
              callback(null, {
                "statusCode": 500,
                "body": err,
                "headers": {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "*"
                }
              });
            } else {
              callback(null, {
                "statusCode": 200,
                "body": JSON.stringify(res),
                "headers": {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "*"
                }
              });
            }
          });
        }))
        .catch(err => {
          callback(null, {
            "statusCode": 200,
            "body": JSON.stringify(err),
            "headers": {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "*"
            }
          });
        });
    } else {
      let params = {
        DeliveryStreamName: process.env.DELIVERY_STREAM_NAME,
        Record: {
            Data: `${JSON.stringify(data)}\n`
        }
      };
      firehose.putRecord(params, (err, res) => {
        if (err) {
          callback(null, {
            "statusCode": 500,
            "body": err,
            "headers": {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "*"
            }
          });
        } else {
          callback(null, {
            "statusCode": 200,
            "body": JSON.stringify(res),
            "headers": {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "*"
            }
          });
        }
      });
    }
  } else {
    callback(null, {
      "statusCode": 200,
      "body": "",
      "headers": {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "*"
      }
    });
  }
};
