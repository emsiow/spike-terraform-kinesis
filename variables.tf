variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}
variable "aws_account_id" {}
variable "ipdata_api_key" {}
variable "userstack_api_key" {}
